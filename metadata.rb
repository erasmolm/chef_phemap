name 'phemap_cubietruck'
maintainer 'Erasmo La Montagna'
maintainer_email 'you@example.com'
license 'all_rights'
description 'Installs/Configures PHEMAP pufLess package'
long_description 'Installs/Configures PHEMAP pufLess package'
version '0.4.7'
# issues_url 'https://github.com/learn-chef/learn_chef_apache2/issues' if respond_to?(:issues_url)
# source_url 'https://github.com/learn-chef/learn_chef_apache2' if respond_to?(:source_url)
